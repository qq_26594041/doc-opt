package com.du.utils;


import org.apache.poi.ss.usermodel.*;
import org.springframework.util.ReflectionUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.List;

/**
 * @author 杜瑞龙
 * @date 2021/7/6 21:09
 */
public class ExcelUtils {

    /**
     * 写入excel数据
     *
     * @param path       excel文件路径
     * @param data       待写入的数据
     * @param firstIndex 开始操作的行数
     * @param isAppend   是否追加，true：在原表格后追加数据；false：覆写原表格数据
     * @return
     */
    public static void writeToExcel(String path, List<?> data, int firstIndex, boolean isAppend) throws Exception {
        try (FileInputStream fis = new FileInputStream(path)) {
            Workbook workbook = WorkbookFactory.create(fis);
            //对excel文档的第一页,即sheet1进行操作
            Sheet sheet = workbook.getSheetAt(0);
            //设置单元格样式
            CellStyle style = workbook.createCellStyle();
            //向excel中写数据
            try (FileOutputStream out = new FileOutputStream(path)) {
                Row row;
                int rowNum = isAppend ? sheet.getLastRowNum() + 1 + firstIndex : firstIndex;
                for (int i = 0; i < data.size(); i++) {
                    Field[] fields = data.get(i).getClass().getDeclaredFields();
                    row = sheet.createRow(i + rowNum);
                    for (int j = 0; j < fields.length; j++) {
                        ReflectionUtils.makeAccessible(fields[j]);
                        Object resultValue = fields[j].get(data.get(i));
                        if (resultValue != null) {
                            Class<?> type = resultValue.getClass();
                            Cell cell = row.createCell(j);
                            cell.setCellStyle(style);
                            if (type.isPrimitive()) {
                                cell.setCellValue(resultValue + "");
                            } else {
                                cell.setCellValue(resultValue.toString());
                            }
                        }
                    }
                }
                out.flush();
                workbook.write(out);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
