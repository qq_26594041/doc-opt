package com.du.write;


import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Calendar;
import java.util.Date;

import static org.apache.poi.ss.usermodel.CellType.*;

/**
 * @author 杜瑞龙
 * @date 2021/7/13 20:11
 */
public class WriteExcelTest {


    @Test
    public void  testHSSFOrXSSF() throws IOException {
        Workbook wb =new HSSFWorkbook();
        /**
         *  sheet
         */
        Sheet sheet = wb.createSheet("new sheet");
        Sheet sheet2 = wb.createSheet("second sheet");
// Note that sheet name is Excel must not exceed 31 characters
// and must not contain any of the any of the following characters:
// 0x0000
// 0x0003
// colon (:)
// backslash (\)
// asterisk (*)
// question mark (?)
// forward slash (/)
// opening square bracket ([)
// closing square bracket (])
// You can use org.apache.poi.ss.util.WorkbookUtil#createSafeSheetName(String nameProposal)}
// for a safe way to create valid names, this utility replaces invalid characters with a space (' ')
        String safeName = WorkbookUtil.createSafeSheetName("[O'Brien's sales*?]"); // returns " O'Brien's sales   "
        Sheet sheet3 = wb.createSheet(safeName);

/**
 * cell
 */

        CreationHelper createHelper = wb.getCreationHelper();


        Row row = sheet.createRow(0);
// Create a cell and put a value in it.
        Cell cell = row.createCell(0);
        cell.setCellValue(1);
// Or do it on one line.
        row.createCell(1).setCellValue(1.2);
        row.createCell(2).setCellValue(
                createHelper.createRichTextString("This is a string"));
        row.createCell(3).setCellValue(true);


/**
 * date
 */
        // Create a row and put some cells in it. Rows are 0 based.
        Row row1 = sheet.createRow(1);
// Create a cell and put a date value in it.  The first cell is not styled
// as a date.
        Cell cell1 = row1.createCell(0);
        cell1.setCellValue(new Date());
// we style the second cell as a date (and time).  It is important to
// create a new cell style from the workbook otherwise you can end up
// modifying the built in style and effecting not only this cell but other cells.
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setDataFormat(
                createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
        cell = row1.createCell(1);
        cell.setCellValue(new Date());
        cell.setCellStyle(cellStyle);
//you can also set date as java.util.Calendar
        cell = row.createCell(2);
        cell.setCellValue(Calendar.getInstance());
        cell.setCellStyle(cellStyle);

/**
 * 数据类型
 */
        Row row2 = sheet.createRow(3);
        row2.createCell(0).setCellValue(1.1);
        row2.createCell(1).setCellValue(new Date());
        row2.createCell(2).setCellValue(Calendar.getInstance());
        row2.createCell(3).setCellValue("a string");
        row2.createCell(4).setCellValue(true);
        row2.createCell(5).setCellType(ERROR);

/**
 *
 */













        String property = System.getProperty("user.dir")+"/workbook.xls";
        try (OutputStream fileOut = new FileOutputStream(property)) {
            wb.write(fileOut);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            wb.close();
        }
    }


    @Test
    public  void fileSystem() throws IOException, InvalidFormatException {
//        Files vs InputStreams
//        When opening a workbook, either a .xls HSSFWorkbook, or a .xlsx XSSFWorkbook,
//        the Workbook can be loaded from either a File or an InputStream.
//        Using a File object allows for lower memory consumption, w
//        hile an InputStream requires more memory as it has to buffer the whole file.
//   If using WorkbookFactory, it's very easy to use one or the other


        // Use a file
        Workbook wb = WorkbookFactory.create(new File("MyExcel.xls"));
// Use an InputStream, needs more memory
        Workbook wbStream = WorkbookFactory.create(new FileInputStream("MyExcel.xlsx"));


 //      If using HSSFWorkbook or XSSFWorkbook directly
 //       you should generally go through POIFSFileSystem or OPCPackage,
 //      to have full control of the lifecycle (including closing the file when done):

// HSSFWorkbook, File
        POIFSFileSystem fs = new POIFSFileSystem(new File("file.xls"));
        HSSFWorkbook wb2 = new HSSFWorkbook(fs.getRoot(), true);
//....
        fs.close();
// HSSFWorkbook, InputStream, needs more memory
        FileInputStream myInputStream = new FileInputStream("");
        POIFSFileSystem fs1 = new POIFSFileSystem(myInputStream);
        HSSFWorkbook wb3 = new HSSFWorkbook(fs.getRoot(), true);
// XSSFWorkbook, File
        OPCPackage pkg = OPCPackage.open(new File("file.xlsx"));
        XSSFWorkbook wb5 = new XSSFWorkbook(pkg);
//....
        pkg.close();
// XSSFWorkbook, InputStream, needs more memory
        OPCPackage pkg4 = OPCPackage.open(myInputStream);
        XSSFWorkbook wb9 = new XSSFWorkbook(pkg);
//....
        pkg.close();
    }

    @Test
    public void demonstratesVariousAlignmentOpt() throws IOException {
        Workbook wb = new XSSFWorkbook(); //or new HSSFWorkbook();
        Sheet sheet = wb.createSheet();
        Row row = sheet.createRow(2);
        row.setHeightInPoints(30);
        createCell(wb, row, 0, HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
        createCell(wb, row, 1, HorizontalAlignment.CENTER_SELECTION, VerticalAlignment.BOTTOM);
        createCell(wb, row, 2, HorizontalAlignment.FILL, VerticalAlignment.CENTER);
        createCell(wb, row, 3, HorizontalAlignment.GENERAL, VerticalAlignment.CENTER);
        createCell(wb, row, 4, HorizontalAlignment.JUSTIFY, VerticalAlignment.JUSTIFY);
        createCell(wb, row, 5, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
        createCell(wb, row, 6, HorizontalAlignment.RIGHT, VerticalAlignment.TOP);
        // Write the output to a file
        String property = System.getProperty("user.dir")+"/workbook.xls";
        try (OutputStream fileOut = new FileOutputStream(property)) {
            wb.write(fileOut);
        }
        wb.close();

    }

    /**
     * Creates a cell and aligns it a certain way.
     *
     * @param wb     the workbook
     * @param row    the row to create the cell in
     * @param column the column number to create the cell in
     * @param halign the horizontal alignment for the cell.
     * @param valign the vertical alignment for the cell.
     */
    private static void createCell(Workbook wb, Row row, int column, HorizontalAlignment halign, VerticalAlignment valign) {
        Cell cell = row.createCell(column);
        cell.setCellValue("Align It");
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(halign);
        cellStyle.setVerticalAlignment(valign);
        cell.setCellStyle(cellStyle);
    }

    @Test
    public  void workingWithBorders() throws IOException {
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("new sheet");
// Create a row and put some cells in it. Rows are 0 based.
        Row row = sheet.createRow(1);
// Create a cell and put a value in it.
        Cell cell = row.createCell(1);
        cell.setCellValue(4);
// Style the cell with borders all around.
        CellStyle style = wb.createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.RED.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.GREEN.getIndex());
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLUE.getIndex());
        style.setBorderTop(BorderStyle.MEDIUM_DASHED);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        cell.setCellStyle(style);
// Write the output to a file
        String property = System.getProperty("user.dir")+"/workbook.xls";
        try (OutputStream fileOut = new FileOutputStream(property)) {
            wb.write(fileOut);
        } catch (IOException e) {
            e.printStackTrace();
        }
        wb.close();

    }
    /**
     * Iterate over cells, with control of missing / blank cells
     * In some cases, when iterating, you need full control over how missing or blank rows and cells are treated,
     * and you need to ensure you visit every cell and not just those defined in the file.
     * (The CellIterator will only return the cells defined in the file, which is largely those with values or stylings,
     * but it depends on Excel).
     *
     * In cases such as these, you should fetch the first and last column information for a row,
     * then call getCell(int, MissingCellPolicy) to fetch the cell. Use a MissingCellPolicy to control how blank or null cells are handled.
     */

    @Test
    public void  missingOrBlank(){
//        Workbook wb = new HSSFWorkbook();
//        Sheet sheet = wb.createSheet("new sheet");
//// Decide which rows to process
//        int rowStart = Math.min(15, sheet.getFirstRowNum());
//        int rowEnd = Math.max(1400, sheet.getLastRowNum());
//        for (int rowNum = rowStart; rowNum < rowEnd; rowNum++) {
//            Row r = sheet.getRow(rowNum);
//            if (r == null) {
//                // This whole row is empty
//                // Handle it as needed
//                continue;
//            }
//            int lastColumn = Math.max(r.getLastCellNum(), MY_MINIMUM_COLUMN_COUNT);
//            for (int cn = 0; cn < lastColumn; cn++) {
//                Cell c = r.getCell(cn, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
//                if (c == null) {
//                    // The spreadsheet is empty in this cell
//                } else {
//                    // Do something useful with the cell's contents
//                }
//            }
//        }
    }

    @Test
    public void getCell() throws IOException {

        String property = System.getProperty("user.dir")+"/workbook.xls";
        POIFSFileSystem fs = new POIFSFileSystem(new File(property));
        HSSFWorkbook wb = new HSSFWorkbook(fs.getRoot(), true);
        DataFormatter formatter
                = new DataFormatter();
        Sheet sheet1 = wb.getSheetAt(0);
        for (Row row : sheet1) {
            for (Cell cell : row) {
                CellReference cellRef = new CellReference(row.getRowNum(), cell.getColumnIndex());
                System.out.print(cellRef.formatAsString());
                System.out.print(" - ");
                // get the text that appears in the cell by getting the cell value and applying any data formats (Date, 0.00, 1.23e9, $1.23, etc)
                String text = formatter.formatCellValue(cell);
                System.out.println(text);
                // Alternatively, get the value and format it yourself
                switch (cell.getCellType()) {
                    case STRING:
                        System.out.println(cell.getRichStringCellValue().getString());
                        break;
                    case NUMERIC:
                        if (DateUtil.isCellDateFormatted(cell)) {
                            System.out.println(cell.getDateCellValue());
                        } else {
                            System.out.println(cell.getNumericCellValue());
                        }
                        break;
                    case BOOLEAN:
                        System.out.println(cell.getBooleanCellValue());
                        break;
                    case FORMULA:
                        System.out.println(cell.getCellFormula());
                        break;
                    case BLANK:
                        System.out.println();
                        break;
                    default:
                        System.out.println();
                }
            }
        }
        fs.close();
    }

    /**
     * Text Extraction
     *     For most text extraction requirements, the standard ExcelExtractor class should provide all you need.
     *     For very fancy text extraction, XLS to CSV etc, take a look at /src/examples/src/org/apache/poi/examples/hssf/eventusermodel/XLS2CSVmra.java
     */
    @Test
    public void textExtraction(){
        String property = System.getProperty("user.dir")+"/workbook.xls";
        try (InputStream inp = new FileInputStream(property)) {
            HSSFWorkbook wb = new HSSFWorkbook(new POIFSFileSystem(inp));
            ExcelExtractor extractor = new ExcelExtractor(wb);
            extractor.setFormulasNotResults(true);
            extractor.setIncludeSheetNames(false);
            String text = extractor.getText();
            wb.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Fills and colors
     */
    @Test
    public void  fillsAndColors() throws IOException {

        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("new sheet");
// Create a row and put some cells in it. Rows are 0 based.
        Row row = sheet.createRow(1);
// Aqua background
        CellStyle style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
        style.setFillPattern(FillPatternType.BIG_SPOTS);
        Cell cell = row.createCell(1);
        cell.setCellValue("X");
        cell.setCellStyle(style);
// Orange "foreground", foreground being the fill foreground not the font color.
        style = wb.createCellStyle();
        style.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cell = row.createCell(2);
        cell.setCellValue("X");
        cell.setCellStyle(style);
// Write the output to a file
        String property = System.getProperty("user.dir")+"/workbook.xls";
        try (OutputStream fileOut = new FileOutputStream(property)) {
            wb.write(fileOut);
        }
        wb.close();
    }
    /**
     * Merging cells
     */
    @Test
    public void  mergingCells() throws IOException {

        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("new sheet");
        Row row = sheet.createRow(1);
        Cell cell = row.createCell(1);
        cell.setCellValue("This is a test of merging");
        sheet.addMergedRegion(new CellRangeAddress(
                1, //first row (0-based)
                1, //last row  (0-based)
                1, //first column (0-based)
                2  //last column  (0-based)
        ));
// Write the output to a file
        String property = System.getProperty("user.dir")+"/workbook.xls";
        try (OutputStream fileOut = new FileOutputStream(property)) {
            wb.write(fileOut);
        } catch (IOException e) {
            e.printStackTrace();
        }
        wb.close();
    }
    /**
     * Working with fonts
     * Note, the maximum number of unique fonts in a workbook is limited to 32767.
     * You should re-use fonts in your applications instead of creating a font for each cell.
     *  Examples:
     *     Wrong:
             *            for (int i = 0; i < 10000; i++) {
             *     Row row = sheet.createRow(i);
             *     Cell cell = row.createCell(0);
             *     CellStyle style = workbook.createCellStyle();
             *     Font font = workbook.createFont();
             *     font.setBoldweight(Font.BOLDWEIGHT_BOLD);
             *     style.setFont(font);
             *     cell.setCellStyle(style);
     *
     *   Correct:
     *
     *    CellStyle style = workbook.createCellStyle();
            Font font = workbook.createFont();
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            style.setFont(font);
            for (int i = 0; i < 10000; i++) {
                Row row = sheet.createRow(i);
                Cell cell = row.createCell(0);
                cell.setCellStyle(style);
           }
     *
     */
    @Test
    public void workingWithFonts() throws IOException {

        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("new sheet");
// Create a row and put some cells in it. Rows are 0 based.
        Row row = sheet.createRow(1);
// Create a new font and alter it.
        Font font = wb.createFont();
        font.setFontHeightInPoints((short)24);
        font.setFontName("Courier New");
        font.setItalic(true);
        font.setStrikeout(true);
        font.setBold(true);
// Fonts are set into a style so create a new one to use.
        CellStyle style = wb.createCellStyle();
        style.setFont(font);
// Create a cell and put a value in it.
        Cell cell = row.createCell(1);
        cell.setCellValue("This is a test of fonts");
        cell.setCellStyle(style);
// Write the output to a file
        String property = System.getProperty("user.dir")+"/workbook.xls";
        try (OutputStream fileOut = new FileOutputStream(property)) {
            wb.write(fileOut);
        }
        wb.close();
    }


}
