package com.du.write;

import cn.hutool.core.io.IoUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicInteger;

/**
  *
  * @date 2021/12/2 16:10
  * @author  杜瑞龙
 */
public class Oscar{

    @Test
    public void  test() throws IOException {
//        String s = HttpUtil.get("https://oscar.wmo.int/surface//rest/api/search/station?");
//        JSONObject jsonObject = JSONUtil.parseObj(s);
        JSONArray stationSearchResults = JSONUtil.readJSONArray(new File("C:\\Users\\du\\Desktop\\osar.json"), StandardCharsets.UTF_8);
     //   JSONArray stationSearchResults = jsonObject.getJSONArray("stationSearchResults");
//        FileOutputStream fileOutputStream = new FileOutputStream("");
//        IoUtil.writeUtf8(fileOutputStream,true,stationSearchResults);
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet oscar = workbook.createSheet("oscar");
        AtomicInteger i = new AtomicInteger();
        XSSFRow row = oscar.createRow(i.get());
        row.createCell(0, CellType.STRING).setCellValue("id");
        row.createCell(1, CellType.STRING).setCellValue("region");
        row.createCell(2, CellType.STRING).setCellValue("elevation");
        row.createCell(3, CellType.STRING).setCellValue("latitude");
        row.createCell(4, CellType.STRING).setCellValue("longitude");
        row.createCell(5, CellType.STRING).setCellValue("declaredStatus");
        row.createCell(6, CellType.STRING).setCellValue("wigosId");
        row.createCell(7, CellType.STRING).setCellValue("stationProgramsDeclaredStatuses");
        row.createCell(8, CellType.STRING).setCellValue("stationTypeName");
        row.createCell(9, CellType.STRING).setCellValue("stationTypeId");
        row.createCell(10, CellType.STRING).setCellValue("stationStatusCode");
        row.createCell(11, CellType.STRING).setCellValue("name");
        row.createCell(12, CellType.STRING).setCellValue("wigosStationIdentifiers");
        row.createCell(13, CellType.STRING).setCellValue("territory");
        row.createCell(14, CellType.STRING).setCellValue("dateEstablished");
        row.createCell(15, CellType.STRING).setCellValue("stationTypeCode");


        for (int i1 = 0; i1 < stationSearchResults.size(); i1++) {

            XSSFRow row2 = oscar.createRow(i.incrementAndGet());
            JSONObject jsonObject1 = stationSearchResults.getJSONObject(i1);
            row2.createCell(0, CellType.STRING).setCellValue(jsonObject1.getStr("id"));
            row2.createCell(1, CellType.STRING).setCellValue(jsonObject1.getStr("region"));
            row2.createCell(2, CellType.STRING).setCellValue(jsonObject1.getStr("elevation"));
            row2.createCell(3, CellType.STRING).setCellValue(jsonObject1.getStr("latitude"));
            row2.createCell(4, CellType.STRING).setCellValue(jsonObject1.getStr("longitude"));
            row2.createCell(5, CellType.STRING).setCellValue(jsonObject1.getStr("declaredStatus"));
            row2.createCell(6, CellType.STRING).setCellValue(jsonObject1.getStr("wigosId"));

            row2.createCell(7, CellType.STRING).setCellValue(jsonObject1.getStr("stationProgramsDeclaredStatuses"));
            row2.createCell(8, CellType.STRING).setCellValue(jsonObject1.getStr("stationTypeName"));
            row2.createCell(9, CellType.STRING).setCellValue(jsonObject1.getStr("stationTypeId"));
            row2.createCell(10, CellType.STRING).setCellValue(jsonObject1.getStr("stationStatusCode"));
            row2.createCell(11, CellType.STRING).setCellValue(jsonObject1.getStr("name"));
            row2.createCell(12, CellType.STRING).setCellValue(
                    jsonObject1.getOrDefault("wigosStationIdentifiers", "").toString());
            row2.createCell(13, CellType.STRING).setCellValue(jsonObject1.getStr("territory"));
            row2.createCell(14, CellType.STRING).setCellValue(jsonObject1.getStr("dateEstablished"));
            row2.createCell(15, CellType.STRING).setCellValue(jsonObject1.getStr("stationTypeCode"));


        }
        try (OutputStream fileOut = new FileOutputStream("C:\\Users\\du\\Desktop\\osar.xlsx")) {
            workbook.write(fileOut);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            workbook.close();
        }
    }
}
