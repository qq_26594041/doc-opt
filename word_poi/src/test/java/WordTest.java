import cn.hutool.core.util.IdUtil;
import org.apache.poi.ooxml.POIXMLDocumentPart;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.openxml4j.opc.PackageRelationshipCollection;
import org.apache.poi.openxml4j.opc.internal.ContentType;
import org.apache.poi.util.StringUtil;
import org.apache.poi.xwpf.usermodel.*;
import org.junit.jupiter.api.Test;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author 杜瑞龙
 * @date 2021/7/17 17:59
 */
public class WordTest {

    @Test
    public void testWord() throws IOException, OpenXML4JException {
        InputStream is = new FileInputStream("C:\\Users\\duruilongS\\Desktop\\protal数据注册附件版.docx");
        XWPFDocument doc = new XWPFDocument(is);
        List<PackagePart> allEmbeddedParts = doc.getAllEmbeddedParts();

        List<XWPFPictureData> allPictures = doc.getAllPictures();

        for (XWPFPictureData allPicture : allPictures) {

            String fileName = allPicture.getFileName();
            byte[] data = allPicture.getData();
        }

        List<XWPFTable> tables = doc.getTables();
        for (XWPFTable table : tables) {
            List<XWPFTableRow> rows = table.getRows();
            for (XWPFTableRow row : rows) {
                for (XWPFTableCell tableCell : row.getTableCells()) {
                    String text = tableCell.getText();
                    List<IBodyElement> bodyElements = tableCell.getBodyElements();
                    List<XWPFParagraph> paragraphs = tableCell.getParagraphs();
                    System.out.println();
                }
            }
        }


    }
}
