//package com.du.bean;
//
//import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
//import com.alibaba.excel.annotation.ExcelProperty;
//import lombok.AllArgsConstructor;
//import lombok.Getter;
//import lombok.Setter;
//
//import java.math.BigDecimal;
//import java.sql.Timestamp;
//
///**
// * 站点/平台(tab_omin_cm_cc_stationplat)
// *
// * @author bianj
// * @version 1.0.0 2021-03-01
// */
//@Getter
//@Setter
//@AllArgsConstructor
//public class TabOminCmCcStationplat implements java.io.Serializable {
//    /** 版本号 */
//    private static final long serialVersionUID = 4863014471201942743L;
//
//    /** 站点/平台UUID */
//    @ExcelProperty("平台UUID")
//    private String cSiteopfId;
//
//    /** 语种 */
//    private String cMdlang;
//
//    /** 字符集 */
//    private String cMdchar;
//
//    /** 创建日期 */
//    private String cMddatest;
//
//    /** 标准名称 */
//    private String cMdstanname;
//
//    /** 标准版本 */
//    private String cMdstanver;
//
//    /** 区站号 */
//    private String cIndexnbr;
//
//    /** 子站号 */
//    private String cIndexsubnbr;
//
//    /** 台站/平台唯一标识符 */
//    private String cSiteopfFlag;
//
//    /** 台站档案号 */
//    private String cArchiveinbr;
//
//    /** 站名（中文） */
//    private String cStationnc;
//
//    /** 站名（英文） */
//    private String cStationne;
//
//    /** 站别名 */
//    private String cAlias;
//
//    /** 台站/平台模式 */
//    private String cSiteopfModel;
//
//    /** 台站/平台类型 */
//    private String cSiteopfType;
//
//    /** 经度 */
//    private String cLongitude;
//
//    /** 纬度 */
//    private String cLatitude;
//
//    /** 气压表海拔高度 */
//    private String cHp;
//
//    /** 测站海拔高度 */
//    private String cHha;
//
//    /** 地理环境 */
//    private String cEnvironment;
//
//    /** WMO区协代码 */
//    private String cRegionid;
//
//    /** 时区 */
//    private String cTimezone;
//
//    /** 气候带 */
//    private String cClimatezone;
//
//    /** 国家（地区）名称 */
//    private String cCountryid;
//
//    /** 区域中心代码 */
//    private String cRegionalhc;
//
//    /** 行政区划代码 */
//    private String cAdministrativedc;
//
//    /** 详细地址 */
//    private String cAddress;
//
//    /** 台站URL */
//    private String cStationurl;
//
//    /** 其他链URL */
//    private String cOtherurl;
//
//    /** 建站时间 */
//    private String cEstadate;
//
//    /** 撤站时间 */
//    private String cRepdate;
//
//    /** 资料通信方法 */
//    private String cCmethod;
//
//    /** 台站状态 */
//    private String cStatus;
//
//    /** 站网信息 */
//    private String cSiteinformation;
//
//    /** 其它 */
//    private String cAppendix;
//
//    /** 是否考核 */
//    private String cIsexam;
//
//    /** 乡镇 */
//    private String cTownship;
//
//    /** 所属机构 */
//    private String cOrganization;
//
//    /** 洲代码 */
//    private String cContinent;
//
//    /** 洋代码 */
//    private String cOcean;
//
//    /** 浮点经度 */
//    private BigDecimal cLongitudeNumb;
//
//    /** 浮点纬度 */
//    private BigDecimal cLatitudeNumb;
//
//    /** 管理台站 */
//    private String cManastation;
//
//    /** 资金来源 */
//    private String cFundssources;
//
//    /** 站点特殊类别 */
//    private String cSpecifictype;
//
//    /** 疑误信息反馈单位 */
//    private String cInfofeedbackdep;
//
//    /** 设备通信模块ID号 */
//    private String cModuleid;
//
//    /** 风速感应器距地高度 */
//    private String cWindsensorheight;
//
//    /** 测站类别 */
//    private String cStationtype;
//
//    /** 是否上传分钟数据文件 */
//    private String cIsuploadminutedata;
//
//    /** 是否上传国家站小时数据文件 */
//    private String cIsuploadhourdata;
//
//    /** 是否上传日数据文件 */
//    private String cIsuploaddaydata;
//
//    /** 是否上传日照数据文件 */
//    private String cIsuploadsunshinedata;
//
//    /** 是否上传小时辐射数据 */
//    private String cIsuploadradiatehourdata;
//
//    /** 台站数据处理人员电话 */
//    private String cTeldataprocesser;
//
//    /** 台站管理人员电话 */
//    private String cTelmanager;
//
//    /** 备注 */
//    private String cRemarks;
//
//    /** 台站简称 */
//    private String cShortName;
//
//    /** 风速传感器平台距地高度 */
//    private String cWindSensorHeight;
//
//    /** 省简称 */
//    private String cProvinceShort;
//
//    /** 市简称 */
//    private String cCityShort;
//
//    /** 县简称 */
//    private String cCountyShort;
//
//    /** 设备ID */
//    private String cEquId;
//
//    /** 要素数 */
//    private String cObgNum;
//
//    /** 是否上传区域站小时数据文件 */
//    private String cIsuploadreghourdata;
//
//    /** 是否上传酸雨日数据文件 */
//    private String cIsuploadacidraindaydata;
//
//    /** 国家站地面天气站(是/否) */
//    private String cIsweatherstation;
//
//    /** 部门 */
//    private String cDept;
//
//    /** 是否是高山站 */
//    private String cIfMountain;
//
//    /** 元数据更新时间 */
//    private String cMdupdate;
//
//    /** 是否是交通站 */
//    private String cIstrafficstation;
//
//    /** 是否是旅游站 */
//    private String cIstouriststation;
//
//    /** 是否山洪站 */
//    private String cIsfloodstation;
//
//    /** 创建时间 */
//    private Timestamp cCreateDate;
//
//    /** 更新时间 */
//    private Timestamp cUpdatedDate;
//
//    /** 最近更新人 */
//    private String cModifier;
//
//    /** 省（自治区、直辖市、特别行政区）名称 */
//    private String cCountry;
//
//    /** 省份名称-值 */
//    private String cCountryName;
//
//    /** 地级市（地区、自治州、盟）名称 */
//    private String cCity;
//
//    /** 市区名称-值 */
//    private String cCityName;
//
//    /** 县（市辖区、县级市、自治县、旗、自治旗、特区、林区）名称 */
//    private String cCounty;
//
//    /** 县城名称-值 */
//    private String cCountyName;
//
//    /** 台（站）长 */
//    private String cChargeperson;
//
//    /** 操作状态 */
//    private String cOptType;
//
//    /** 雷达天线馈源海拔高度 */
//    private String cRaa;
//
//    /** 版本号(自增) */
//    private Integer version;
//
//    /** 国家名称 */
//    private String cCountryname;
//
//    /** A隐藏,B同步,C可见不同步 */
//    private String cMetadataType;
//
//}