package com.du.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author du
 * @date 2020/12/5 21:43
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Component
@AllArgsConstructor
public class ResultVO<Object> implements Serializable {

    private static final long serialVersionUID = -5567158279004917024L;
    private Integer code;
    private String msg;
    private transient Object data;
}
