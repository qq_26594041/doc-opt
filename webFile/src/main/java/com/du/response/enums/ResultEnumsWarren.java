package com.du.response.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 返回值警告的枚举  450 ~ 499
 * @author 杜瑞龙
 * @date 2021/1/4 21:58
 */
@Getter
@RequiredArgsConstructor
public enum ResultEnumsWarren {
    /**
     *
     */
    WARREN(451, "警告"),
    PARAMETER_WARREN(4522, "参数错误"),
    PASSWORD_WARREN(453, "密码错误"),
    NO_ACCESS_AUTHORITY_WARREN(454, "没有访问权限"),
    TOKEN_IS_BLACKLIST_WARREN(456, "此token为黑名单"),
    LOGIN_IS_OVERDUE_WARREN(457, "登录已失效"),
    USER_NO_LOGIN_WARREN(458, "用户未登录"),
    USER_LOGIN_FAILED_WARREN(459, "用户账号或密码错误"),
    ;

    private final Integer code;
    private final String message;
}
