package com.du.response.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 返回结果枚举的封装
 *
 * @author 杜瑞龙
 * @date 2020/12/26 20:22
 */
@Getter
@RequiredArgsConstructor
public enum ResultEnumsInfo {
    /**
     * 返回正确的枚举类 200~399
     */
    SUCCESS(201, "成功"),
    USER_LOGIN_SUCCESS(202, "登录成功"),
    USER_LOGOUT_SUCCESS(203, "用户登出成功"),
    INSERT_SUCCESS(204, "保存成功"),
    UPDATE_SUCCESS(205, "修改成功"),
    SELECT_SUCCESS(206, "查询成功"),
    DELETE_SUCCESS(207, "删除成功"),
    EMPTY_SUCCESS(208, "清空列表成功"),
    IMAGE_UPLOAD_SUCCESS(209, "图片上传成功"),
    ;
    private final Integer code;
    private final String message;

}
