package com.du.response.enums;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 返回值警告的枚举  530-600
 *
 * @author 杜瑞龙
 * @date 2021/1/4 22:02
 */
@Getter
@RequiredArgsConstructor
public enum ResultEnumsError {
    /**
     *
     */
    ERROR(530, "系统异常"),
    SQL_ERROR(531, "sql异常"),
    LOGIN_FAILURE(534, "用户或密码错误");
    private final Integer code;
    private final String message;
}
