package com.du.response;




import com.du.exceptions.enums.LanguageEnumException;
import com.du.exceptions.enums.MyExceptionEnum;
import com.du.response.enums.ResultEnumsError;
import com.du.response.enums.ResultEnumsInfo;
import com.du.response.enums.ResultEnumsWarren;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author du
 * @date 2020/12/27 14:39
 */
@Component
@RequiredArgsConstructor
public class ResultUtils {

    private final ObjectMapper objectMapper;


    /**
     * 成功返回特定的状态码和信息和数据
     *
     */
    public ResultVO<Object> resultSuccess(ResultEnumsInfo respCode, Object data) {
        return getResByResultEnumsInfoWithObject(respCode, data);
    }


    /**
     * 成功返回特定的状态码和信息
     *
     * @param respCode 枚举成功
     * @return {@link ResultVO}
     */
    public static  ResultVO<Object> resultSuccess(ResultEnumsInfo respCode) {
        return getResByResultEnumsInfo(respCode);
    }

    /**
     * 警告返回特定的状态码和信息和数据
     *
     * @param respCode 枚举警告
     * @param data     响应数据
     * @return {@link ResultVO}
     */
    public ResultVO<Object> resultWarren(ResultEnumsWarren respCode, Object data) {
        return getResByResultEnumsWarrenWithObject(respCode, data);
    }


    /**
     * 警告返回特定的状态码和信息
     *
     * @param respCode 枚举警告
     * @return {@link ResultVO}
     */
    public ResultVO<Object> resultWarren(ResultEnumsWarren respCode) {
        return getResByResultEnumsWarren(respCode);
    }

    /**
     * 错误返回特定的状态码和信息和数据
     *
     * @param respCode 枚举错误
     * @param data     响应数据
     * @return {@link ResultVO}
     */
    public ResultVO<Object> resultError(ResultEnumsError respCode, Object data) {
        return getResByResultEnumsErrorWithObject(respCode, data);
    }


    /**
     * 错误返回特定的状态码和信息
     *
     * @param respCode 枚举错误
     * @return {@link ResultVO}
     */
    public ResultVO<Object> resultError(ResultEnumsError respCode) {
        return getResByResultEnumsError(respCode);
    }

    /**
     * 自定义异常返回特定的状态码和信息和数据
     *
     * @param respCode 枚举异常
     * @param data     响应数据
     * @return {@link ResultVO}
     */
    public ResultVO<Object> resultMyException(MyExceptionEnum respCode, Object data) {
        return getResByResultEnumsMyExceptionWithObject(respCode, data);
    }


    /**
     * 自定义异常返回特定的状态码和信息
     *
     * @param respCode 枚举异常
     * @return {@link ResultVO}
     */
    public ResultVO<Object> resultMyException(MyExceptionEnum respCode) {
        return getResByResultEnumsMyException(respCode);
    }

    /**
     * 直接将传入的对象序列化为json，并且写回客户端 多用于下载文件 避免 二次响应
     *
     * @param resultVO 写入的响应内容
     * @param response 响应体
     */
    public static void writeValue(ResultVO<Object> resultVO, HttpServletResponse response)
            throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        response.setContentType("application/json;charset=utf-8");
        mapper.writeValue(response.getOutputStream(), resultVO);
    }

    /**
     * 语法异常返回特定的状态码和信息和数据
     *
     * @param respCode 枚举异常
     * @param data     数据
     * @return {@link ResultVO}
     */
    public ResultVO<Object> resultLanguageEnumException(LanguageEnumException respCode, Object data) {
        return getResByResultEnumsLanguageEnumExceptionWithObject(respCode, data);
    }


    /**
     * 语法异常返回特定的状态码和信息
     *
     * @param respCode 异常枚举
     * @return {@link ResultVO}
     */
    public ResultVO<Object> resultLanguageEnumException(LanguageEnumException respCode) {
        return getResByResultEnumsLanguageEnumException(respCode);
    }

    /**
     * 自定义异常返回特定的状态码和信息
     *
     * @param respCode 枚举异常
     * @return {@link ResultVO}
     */
    public ResultVO<Object> resultMyException(MyExceptionEnum respCode, String msg) {
        return getResByResultEnumsMyException(respCode, msg);
    }

    /**
     * 响应成功返回正确枚举转对象和数据
     *
     * @param respCode 成功枚举
     * @param data     数据
     * @return {@link ResultVO}
     */
    private ResultVO<Object> getResByResultEnumsInfoWithObject(ResultEnumsInfo respCode, Object data) {
        return getResultEntity(respCode.getCode(), respCode.getMessage(), data);
    }

    /**
     * 响应成功返回正确枚举转对象
     *
     * @param respCode 成功枚举
     * @return {@link ResultVO}
     */
    private static ResultVO<Object> getResByResultEnumsInfo(ResultEnumsInfo respCode) {
        return getResultEntity(respCode.getCode(), respCode.getMessage());
    }

    /**
     * 警告成功返回正确枚举转对象和数据
     *
     * @param respCode 警告枚举
     * @param data     数据
     * @return {@link ResultVO}
     */
    private ResultVO<Object> getResByResultEnumsWarrenWithObject(ResultEnumsWarren respCode, Object data) {
        return getResultEntity(respCode.getCode(), respCode.getMessage(), data);
    }

    /**
     * 警告返回正确枚举转对象
     *
     * @param respCode 警告枚举
     * @return {@link ResultVO}
     */
    private ResultVO<Object> getResByResultEnumsWarren(ResultEnumsWarren respCode) {
        return getResultEntity(respCode.getCode(), respCode.getMessage());
    }

    /**
     * 错误返回正确枚举转对象和数据
     *
     * @param respCode 错误枚举
     * @param data     数据
     * @return {@link ResultVO}
     */
    private ResultVO<Object> getResByResultEnumsErrorWithObject(ResultEnumsError respCode, Object data) {
        return getResultEntity(respCode.getCode(), respCode.getMessage(), data);
    }

    /**
     * 错误返回正确枚举转对象
     *
     * @param respCode 错误枚举
     * @return {@link ResultVO}
     */
    private ResultVO<Object> getResByResultEnumsError(ResultEnumsError respCode) {
        return getResultEntity(respCode.getCode(), respCode.getMessage());
    }

    /**
     * 自定义异常返回正确枚举转对象和数据
     *
     * @param respCode 异常枚举
     * @param data     数据
     * @return {@link ResultVO}
     */
    private ResultVO<Object> getResByResultEnumsMyExceptionWithObject(MyExceptionEnum respCode, Object data) {
        return getResultEntity(respCode.getCode(), respCode.getMessage(), data);
    }

    /**
     * 自定义异常返回正确枚举转对象
     *
     * @param respCode 异常枚举
     * @return @link ResultVO}
     */
    private ResultVO<Object> getResByResultEnumsMyException(MyExceptionEnum respCode) {
        return getResultEntity(respCode.getCode(), respCode.getMessage());
    }

    /**
     * 自定义异常返回正确枚举转对象 自定义提示消息
     *
     * @param respCode 异常枚举
     * @return @link ResultVO}
     */
    private ResultVO<Object> getResByResultEnumsMyException(MyExceptionEnum respCode, String msg) {
        return getResultEntity(respCode.getCode(), msg);
    }

    /**
     * 语法异常返回正确枚举转对象和数据
     *
     * @param respCode 异常枚举
     * @param data     数据
     * @return {@link ResultVO}
     */
    private ResultVO<Object> getResByResultEnumsLanguageEnumExceptionWithObject(LanguageEnumException respCode, Object data) {
        return getResultEntity(respCode.getCode(), respCode.getMessage(), data);
    }

    /**
     * 语法异常返回正确枚举转对象
     *
     * @param respCode 异常枚举
     * @return {@link ResultVO}
     */
    private ResultVO<Object> getResByResultEnumsLanguageEnumException(LanguageEnumException respCode) {
        return getResultEntity(respCode.getCode(), respCode.getMessage());
    }

    /**
     * 语法异常返回正确枚举转对象 自定义提示
     *
     * @param respCode 异常枚举
     * @return {@link ResultVO}
     */
    private ResultVO<Object> getResByResultEnumsLanguageEnumException(LanguageEnumException respCode, String msg) {
        return getResultEntity(respCode.getCode(), msg);
    }

    /**
     * 返回实体对象  默认枚举 带数据
     *
     * @param code 枚举code
     * @param msg  枚举msg
     * @param data 接口产生的数据
     * @return ResultVO
     */
    private ResultVO<Object> getResultEntity(Integer code, String msg, Object data) {

        ResultVO<Object> objectResultVO = new ResultVO<>();
        objectResultVO.setData(data);
        objectResultVO.setCode(code);
        objectResultVO.setMsg(msg);
        return objectResultVO;
    }

    /**
     * 返回实体对象  默认枚举 带数据
     *
     * @param code 枚举code
     * @param msg  枚举msg
     * @return ResultVO
     */
    private static ResultVO<Object> getResultEntity(Integer code, String msg) {
        ResultVO<Object> objectResultVO = new ResultVO<>();
        objectResultVO.setCode(code);
        objectResultVO.setMsg(msg);
        return objectResultVO;
    }
}
