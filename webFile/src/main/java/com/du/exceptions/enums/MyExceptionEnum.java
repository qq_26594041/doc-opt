package com.du.exceptions.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 自定义异常的枚举 790~999
 *
 * @author 杜瑞龙
 * @date 2021/1/4 21:49
 */
@Getter
@RequiredArgsConstructor
public enum MyExceptionEnum {
    /**
     *
     */
    UploadFileNullException(790, "上传文件为空，请检查"),
    UploadFileNameException(791, "上传文件名为空，请检查"),
    ImageFormatException(792, "上传的图片格式异常，请检查"),
    FileUploadSizeException(793, "文件上传的大小异常"),
    DownFileException(794, "文件下载异常"),
    FileDeleteException(795, "文件删除异常");
    private final Integer code;

    private final String message;

}
