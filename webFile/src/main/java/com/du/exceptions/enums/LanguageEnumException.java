package com.du.exceptions.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 语言异常枚举类 620~728 ERROR
 *
 * @author 杜瑞龙
 * @date 2021/1/4 21:32
 */
@Getter
@AllArgsConstructor
public enum LanguageEnumException {

    /**
     *
     */
    MissingServletRequestParam(621, "get请求，参数缺少或类型不匹配"),
    handlerNoFound(622, "路径不存在，请检查路径是否正确"),
    RuntimeExceptions(623, "系统异常，操作失败"),
    NUll_EXCEPTION(624, "空指针，请管理员检查日志"),
    handleDuplicateKeyException(625, "数据重复，请检查后提交"),
    ClassCastException(626, "类型转换异常!"),
    ArrayIndexOutOfBoundsException(627, "数组越界异常!"),
    MultipartException(628, "文件上传异常，请检查文件!"),
    FileNotFoundException(629, "文件找不到"),
    BadCredentialsException(630, "没有找到用户名或密码参数");
    private final Integer code;
    private final String message;

}
