package com.du.exceptions.my;


import com.du.mons.exceptions.enums.MyExceptionEnum;

/**
 * 上传文件名相关的异常
 *
 * @author 杜瑞龙
 * @date 2021/1/26 22:11
 */

public class UploadFileNameException extends RuntimeException {
    public UploadFileNameException(String message) {
        super(message);
    }

    public UploadFileNameException() {
        super(MyExceptionEnum.UploadFileNameException.getMessage());
    }
}
