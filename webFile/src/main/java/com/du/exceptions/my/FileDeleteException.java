package com.du.exceptions.my;


import com.du.mons.exceptions.enums.MyExceptionEnum;

/**
 * @author 杜瑞龙
 * @date 2021/1/31 18:06
 */
public class FileDeleteException extends RuntimeException {
    public FileDeleteException(String message) {
        super(message);
    }

    public FileDeleteException() {
        super(MyExceptionEnum.FileDeleteException.getMessage());
    }
}
