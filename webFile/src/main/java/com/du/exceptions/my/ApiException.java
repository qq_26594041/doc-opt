package com.du.exceptions.my;

/**
 * @author du
 * @date 2020/12/27 14:34
 */

public class ApiException extends  RuntimeException {
    private final int code;
    private final String msg;

    public ApiException() {
        this(1001, "接口错误");
    }

    public ApiException(String msg) {
        this(1001, msg);
    }

    public ApiException(int code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
