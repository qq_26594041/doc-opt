package com.du.exceptions.my;


import com.du.mons.exceptions.enums.MyExceptionEnum;

/**
 * 文件上传大小异常
 *
 * @author 杜瑞龙
 * @date 2021/1/27 19:47
 */
public class FileUploadSizeException extends RuntimeException {
    public FileUploadSizeException(String message) {
        super(message);
    }

    public FileUploadSizeException() {
        super(MyExceptionEnum.FileUploadSizeException.getMessage());
    }
}
