package com.du.exceptions.my;


import com.du.mons.exceptions.enums.MyExceptionEnum;

/**
 * 上传的文件为空的异常
 *
 * @author du
 * @date 2021/1/3 21:09
 */

public class UploadFileNullException extends RuntimeException {
    public UploadFileNullException(String message) {
        super(message);
    }

    public UploadFileNullException() {
        super(MyExceptionEnum.UploadFileNullException.getMessage());
    }
}
