package com.du.exceptions.my;


import com.du.mons.exceptions.enums.MyExceptionEnum;

/**
 * @author 杜瑞龙
 * @date 2021/1/27 19:23
 */
public class FileFormatException extends RuntimeException {
    public FileFormatException(String message) {
        super(message);
    }

    public FileFormatException() {
        super(MyExceptionEnum.ImageFormatException.getMessage());
    }
}
