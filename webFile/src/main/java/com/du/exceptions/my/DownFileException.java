package com.du.exceptions.my;


import com.du.mons.exceptions.enums.MyExceptionEnum;

import java.io.IOException;

/**
 * 文件下载异常
 * @author 杜瑞龙
 * @date 2021/1/31 17:50
 */
public class DownFileException extends IOException {
    public DownFileException(String message) {
        super(message);
    }

    public DownFileException() {
        super(MyExceptionEnum.DownFileException.getMessage());
    }
}
