package com.du.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author 杜瑞龙
 * @date 2022/1/13 9:21
 */
@AllArgsConstructor
@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileVo {


    @JsonIgnore
    private String originalFileName;
    @JsonIgnore
    private String fileNameSuffix;
    @JsonIgnore
    private String createFileName;
    private String savePath;
    /**
     * 相对的文件路径  项目部署的根路径
     */
    private String relativeFilePath;
    private String viewPath;
    @JsonIgnore
    private String dirDate;

}
