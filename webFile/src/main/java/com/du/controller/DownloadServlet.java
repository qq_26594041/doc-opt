package com.du.controller;


import com.du.utils.FileDownloadUtils;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author 杜瑞龙
 * @date 2022/1/15 13:15
 */
@WebServlet("/downloadServlet")
public class DownloadServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String filename = request.getParameter("filename");
        ServletContext servletContext = this.getServletContext();
        String realPath = servletContext.getRealPath("/img/" + filename);
        try (FileInputStream fis = new FileInputStream(realPath)) {
            String mimeType = servletContext.getMimeType(filename);//获取文件的mime类型
            response.setHeader("content-type", mimeType);
            String agent = request.getHeader("user-agent");
            filename = FileDownloadUtils.getFileName(agent, filename);
            response.setHeader("content-disposition", "attachment;filename=" + filename);
            ServletOutputStream sos = response.getOutputStream();
            byte[] buff = new byte[1024 * 8];
            int len = 0;
            while ((len = fis.read(buff)) != -1) {
                sos.write(buff, 0, len);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}