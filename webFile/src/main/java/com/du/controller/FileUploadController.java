package com.du.controller;

import cn.hutool.core.text.CharPool;
import com.du.bean.FileVo;
import com.du.config.UploadConfig;
import com.du.enums.ImageConstants;
import com.du.response.ResultUtils;
import com.du.response.ResultVO;
import com.du.response.enums.ResultEnumsInfo;
import com.du.utils.FileUploadUtils;
import com.du.utils.IpUtils;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author du
 * @date 2020/12/28 22:36
 */
@RestController
@RequestMapping("api/FileUploadController")
@RequiredArgsConstructor
public class FileUploadController {

    final List<String> fileSuffixList = ImageConstants.COMMONS_IMG_FORMAT;
    final long IMG_SIZE60M = ImageConstants.IMG_SIZE60M;
    private final FileUploadUtils fileUploadUtils;
    private final UploadConfig uploadConfig;
    private final ResultUtils resultUtils;

    /**
     * 上传
     *
     * @param image 图片二进制
     * @return 图片地址
     */
    @PostMapping("upload/image")
    public ResultVO<Object> imageUpload(@RequestParam("image") MultipartFile image, HttpServletRequest request) {

        FileVo fileVo = fileUploadUtils.
                uploadRerunTomcatVirtual(image, uploadConfig.getSavePathImag(), fileSuffixList, IMG_SIZE60M);
        fileVo.setViewPath(IpUtils.getIP(request) + uploadConfig.getViewImgPathPrefix()
                + fileVo.getDirDate() + CharPool.SLASH + fileVo.getCreateFileName());
        return resultUtils.resultSuccess(ResultEnumsInfo.IMAGE_UPLOAD_SUCCESS, fileVo);
    }
}
