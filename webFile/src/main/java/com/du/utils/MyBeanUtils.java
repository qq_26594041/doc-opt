package com.du.utils;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * BeanUtils的扩展类
 *
 * @author duruilong
 */
public class MyBeanUtils  {

    // 过滤不需要存入数据库的字段
//	private static final ArrayList<String> FILTER_FIELD = new ArrayList<>(
//			Arrays.asList("attachmentData"));


    /**
     * Object对象里面的属性和值转化成Map对象
     *
     * @param obj 任何对象
     * @return Map
     */
    public static Map<String, Object> object2Map(Object obj)
            throws IllegalAccessException {
        Map<String, Object> map = new HashMap<String, Object>();
        Class<?> clazz = obj.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            String fieldName = field.getName();
            Object value = field.get(obj);
            map.put(fieldName, value);
        }
        return map;
    }

    /**
     * 将Object对象里面的属性和值转化成Map对象
     *
     * @param obj 任何对象
     * @return map
     */
    public static Map<String, Object> object2MapDelEmpty(Object obj)
            throws IllegalAccessException {
        Map<String, Object> map = new HashMap<String, Object>();
        Class<?> clazz = obj.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            String fieldName = field.getName();
            Object value = field.get(obj);
            if (MyStringUtils.isObjectEmpty(value)) {
                continue;
            }
            map.put(fieldName, value);
        }
        return map;
    }

    /**
     * getFields获取所有public 属性
     * getDeclaredFields 获取所有声明的属性
     *
     * @param bean 任何对象
     * @return 将某个类及其继承属性全部添加到Map中
     */
    public static Map<String,Object> beanToMapContainFatherAttribute(Object bean) {

        if (bean == null) {
            return null;
        }
        Field[] fields = bean.getClass().getDeclaredFields();
        if (fields.length == 0) {
            return null;
        }
        Map<String, Object> result = new HashMap<>(fields.length);
        for (Field field : fields) {
            boolean accessible = field.isAccessible();
            if (!accessible) {
                field.setAccessible(true);
            }
            //获取属性名称及值存入Map
            String key = field.getName();
            try {
                result.put(key, field.get(bean));
            }catch (IllegalAccessException ignored) {
            }

            //还原属性标识
            field.setAccessible(accessible);
        }

        //获取父类属性
        fields = bean.getClass().getSuperclass().getDeclaredFields();
        if(fields.length == 0){
            return result;
        }

        for(Field field:fields){
            //重置属性可见(而且一般属性都是私有的)，否则操作无效
            boolean accessible = field.isAccessible();
            if(!accessible){
                field.setAccessible(true);
            }

            //获取属性名称及值存入Map
            String key = field.getName();
            try {
                result.put(key, field.get(bean));
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }

            //还原属性标识
            field.setAccessible(accessible);
        }

        return result;
    }
    /**
     * 判断对象是否是null
     *
     * @param obj f
     * @return f
     */
    public static boolean isNull(Object obj) {
        return obj == null;
    }

    /**
     * 判断对象不是null
     * @param obj f
     * @return f
     */
    public static boolean isNotNull(Object obj) {
        return !isNull(obj);
    }

    /**
     * 判断对象是否为空
     * @param obj fg
     * @return f
     */
    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        } else if (obj instanceof CharSequence) {
            return ((CharSequence) obj).length() == 0;
        } else if (obj instanceof Collection) {
            return ((Collection) obj).isEmpty();
        } else if (obj instanceof Map) {
            return ((Map) obj).isEmpty();
        } else if (obj.getClass().isArray()) {
            return Array.getLength(obj) == 0;
        }

        return false;
    }

    /**
     * 判断对象不为空
     * @param obj f
     * @return bool
     */
    public static boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }

}
