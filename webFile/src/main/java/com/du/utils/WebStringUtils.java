package com.du.utils;

import cn.hutool.core.util.CharUtil;

import com.du.bean.FileVo;
import com.du.exceptions.my.UploadFileNameException;
import org.apache.commons.lang3.StringUtils;

/**
 * 常见的string工具类
 *
 * @author du
 * @date 2020/5/1 22:12
 */

public class WebStringUtils {


    /**
     * 获取文件的后缀名
     * 后面可尝试使用正则表达式
     *
     * @param fileName 文件名
     * @return 文件后缀名
     */
    public static FileVo getFileSuffix(String fileName) {

        if (StringUtils.isBlank(fileName)) {
            throw new UploadFileNameException();
        }
        int pointLocation = fileName.lastIndexOf(CharUtil.DOT);
        String fileNameSuffix = fileName.substring(pointLocation + 1);
        if (StringUtils.isBlank(fileNameSuffix)) {
            throw new UploadFileNameException("文件名格式错误，无后缀名");
        }
        String fileNameRel = fileName.substring(0, pointLocation);
        return
                FileVo.builder()
                        .originalFileName(fileNameRel)
                        .fileNameSuffix(fileNameSuffix)
                        .build();

    }


}
