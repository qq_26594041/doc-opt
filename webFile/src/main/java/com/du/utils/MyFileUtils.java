package com.du.utils;


import com.du.exceptions.my.FileDeleteException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author du
 * @date 2020/5/24 11:00
 */
public class MyFileUtils {




    /**
     * 删除单一文件
     *
     * @param filePath 文件所在服务器绝对路径
     */
    public static void deleteFileByAbsolutePath(String filePath) {
        File file = new File(filePath);
        if (file.exists() && !file.delete()) {
            throw new FileDeleteException();
        }


    }
    /**
     * 获取当前类的路径
     *
     * @param o 对象
     * @return 当前类的路径
     */
    public static File getClassRootPath(Object o) {
        return new File(o.getClass().getResource("/").getPath());
    }

    /**
     * 获取项目打包后的 jar中的文件流
     *
     * @param path spring 打包在jar中的路径
     * @return 流
     * @throws IOException 异常
     */
    public static InputStream getInJarPath(String path) throws IOException {
        return new ClassPathResource(path).getInputStream();
    }

    /**
     * 获取当前项目打包后的根路径
     *
     * @return 项目包的根路径
     */
    public static String getPackerJarOrWarPath() {
        try {
            return new File(ResourceUtils.getURL("classpath:").getPath())
                    .getParentFile().getParentFile().getParent();
        } catch (FileNotFoundException e) {
            return "";
        }

    }


    /**
     * 为防止一个目录下面出现太多文件，要使用hash算法打散存储
     *
     * @param filename 文件名，要根据文件名生成存储目录
     * @param savePath 文件存储路径
     * @return 新的存储目录
     */
    private String makePath(String filename, String savePath) {

        int hashcode = filename.hashCode();
        int dir1 = hashcode & 0xf;
        int dir2 = (hashcode & 0xf0) >> 4;
        String dir = savePath + "\\" + dir1 + "\\" + dir2;
        File file = new File(dir);
        if (!file.exists()) {
            file.mkdirs();
        }
        return dir;
    }
    /**
     * 在指定的目录中查找文件
     * @param target
     * @param fileName
     * @return
     */
    public  static List<String> findFile(File target, String fileName){
        ArrayList<String> path = new ArrayList<>();
        if(target==null) {
            return path;
        }
        if(target.isDirectory()){
            File[] files = target.listFiles();
            if(files!=null){
                for(File f: files){
                    findFile(f,fileName);
                }
            }
        }else{
            String name = target.getName().toLowerCase();
            if(name.contains(fileName)){
                path.add(target.getAbsolutePath());
            }
        }
        return path;
    }
//    /**
//     * 文件分割
//     * @param targetFile 待分割文件
//     * @param cutSize 每个文件大小
//     */
//    public static void division(File targetFile, long cutSize,String targetPath) {
//
//        if (targetFile == null) {
//            return;
//        }
//        // 计算总分割的文件数
//        int num = targetFile.length() % cutSize == 0 ? (int) (targetFile.length() / cutSize) : (int) (targetFile.length() / cutSize + 1);
//        try (
//                BufferedInputStream in = new BufferedInputStream(new FileInputStream(targetFile));
//
//        ) {
//            // 构造一个文件输入流
//
//            BufferedOutputStream out = null;
//            byte[] bytes = null;
//            int len = -1;
//            int count = 0;
//
//            //循环次为生成文件的个数
//            for (int i = 0; i < num; i++) {
//
//                out = new BufferedOutputStream(
//                        new FileOutputStream(new File(targetPath+ (i + 1) + "-temp-" + targetFile.getName())));
//
//
//                if (cutSize <= 1024) {
//                    bytes = new byte[(int) cutSize];
//                    count = 1;
//                } else {
//                    bytes = new byte[1024];
//                    count = (int) cutSize / 1024;
//                }
//                //count放在前面避免多度一次
//                while (count > 0 && (len = in.read(bytes)) != -1) {
//                    out.write(bytes, 0, len);
//                    out.flush();
//                    count--;
//                }
//
//                //计算每个文件大小除于1024的余数，决定是否要再读取一次
//                if (cutSize % 1024 != 0) {
//
//                    bytes = new byte[(int) cutSize % 1024];
//                    len = in.read(bytes);
//                    out.write(bytes, 0, len);
//                    out.flush();
//                    out.close();
//                }
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 合并文件
//     * @param es 待合并的文件
//     * @param targetFile 目标文件
//     */
//    public  static void merge(Enumeration<InputStream> es, String targetFile) {
//
//        try (
//                SequenceInputStream sis = new SequenceInputStream(es);
//                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetFile));
//        ) {
//
//            byte[] bytes = new byte[1024];
//            int len = -1;
//            while ((len = sis.read(bytes)) != -1) {
//                bos.write(bytes, 0, len);
//                bos.flush();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

}
