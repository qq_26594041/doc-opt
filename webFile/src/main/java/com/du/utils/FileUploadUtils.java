package com.du.utils;


import com.du.bean.FileVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author du
 * @date 2020/12/28 23:15
 */
@Slf4j
@Component
public class FileUploadUtils {


    /**
     * @param file       文件上传的主体
     * @param saveFilePath 文件保存的路径 默认按日期文件夹存储
     * @param suffixList   待验证文件后缀名的集合
     * @param fileSize     待验证文件的大小
     * @return file对象
     */
    public FileVo uploadRerunTomcatVirtual(MultipartFile file, String saveFilePath,
                                           List<String> suffixList, long fileSize) {

        FileVo fileVo = FileVo.builder().build();
        try {
            //文件非空检验
            WebFileUtils.checkNull(file);
            String originalFilename = file.getOriginalFilename();
            //获取文件名和后缀名，并校验基本参数
            fileVo = WebStringUtils.getFileSuffix(originalFilename);
            fileVo.setSavePath(saveFilePath);
            //文件类型校验
            MyStringUtils.checkFileSuffix(suffixList,fileVo.getFileNameSuffix());
            //上传文件大小的校验
            MyStringUtils.checkUploadFileSize(file.getSize(), fileSize);
            File uploadPathByDateAndYml = WebFileUtils.createUploadPathByDateAndYml(fileVo);
            file.transferTo(uploadPathByDateAndYml);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileVo;
    }

}
