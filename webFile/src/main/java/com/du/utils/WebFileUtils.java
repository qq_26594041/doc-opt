package com.du.utils;



import com.du.bean.FileVo;
import com.du.exceptions.my.UploadFileNullException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

import static cn.hutool.core.text.CharPool.*;


/**
 * @author du
 * &#064;date  2020/5/24 11:00
 */

public class WebFileUtils {

    private WebFileUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 上传文件的内容非空校验/或者
     */
    public static void checkNull(MultipartFile multipartFile) {
        if (multipartFile.isEmpty()) {

            throw new UploadFileNullException();
        }
    }
    /**
     * 生成文件上传的目录
     *
     * @param fileVo 文件实体
     * @return fileVo
     */
    public static File createUploadPathByDateAndYml(FileVo fileVo) {


        String currentDateChines24 = MyDateUtils.getCurrentDateHms();
        String currentDate12 = MyDateUtils.getCurrentDate12();
        String fileParPath =MyFileUtils.getPackerJarOrWarPath() + fileVo.getSavePath() + currentDate12;
        String createFileName = fileVo.getOriginalFileName() + DASHED  +
                DASHED + currentDateChines24 + DOT + fileVo.getFileNameSuffix();
        File file = new File(fileParPath, createFileName);
        file.mkdirs();
        fileVo.setRelativeFilePath(fileVo.getSavePath() + currentDate12 + SLASH + createFileName);
        fileVo.setCreateFileName(createFileName);
        fileVo.setSavePath(file.getPath());
        fileVo.setDirDate(currentDate12);
        return file;
    }
}
