package com.du.utils;

import cn.hutool.extra.servlet.ServletUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * @author du
 * @date 2020/10/8 20:29
 */
public class MyServletUtil extends ServletUtil {

    /**
     * 对含有路径参数的处理
     *
     * @param path 路径
     * @param args 可变参数的值，
     * @return 真正的路径
     */
    public static String paramChange(String path, String... args) {
        for (String arg : args) {
            path = path.replaceFirst("\\{[^}]*}", arg);
        }
        return path;
    }

    /**
     *
     * @param agent
     * @param filename
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String getFileName(String agent, String filename) throws UnsupportedEncodingException {
        if (agent.contains("MSIE")) {
// IE浏览器
            filename = URLEncoder.encode(filename, "utf-8");
            filename = filename.replace("+", " ");
        } else if (agent.contains("Firefox")) {
// 火狐浏览器


            filename = "=?utf-8?B?" + URLEncoder.encode(filename, "utf-8") + "?=";
        } else {
// 其它浏览器
            filename = URLEncoder.encode(filename, StandardCharsets.UTF_8);
        }
        return filename;
    }
}

