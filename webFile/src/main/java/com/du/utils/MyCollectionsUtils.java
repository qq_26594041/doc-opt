package com.du.utils;


import java.util.*;


/**
 *
 * @author Administrator
 *
 */
public class MyCollectionsUtils  {


  private static Collections collections;

  /**
   * map转为驼峰命名
   *
   * @param map d
   * @return d
   */
  public static Map<String, Object> mapToCamel(Map<String, Object> map) {
    HashMap<String, Object> hashMap = new HashMap<>();
    Set<String> keySet = map.keySet();
    for (String next : keySet) {
      hashMap.put(MyStringUtils.underlineToCamel(next, true),
              map.get(next));
    }
    return hashMap;

  }

  /**
   * listMap转为驼峰命名
   *
   * @param mapList d
   * @return d
   */
  public static List<Map<String, Object>> listMapToCamel(List<Map<String, Object>> mapList) {
    ArrayList<Map<String, Object>> arrayList = new ArrayList<>();
    for (Map<String, Object> map : mapList) {
      arrayList.add(mapToCamel(map));
    }
    return arrayList;
  }

}
