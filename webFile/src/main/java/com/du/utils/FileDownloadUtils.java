package com.du.utils;


import cn.hutool.core.codec.Base64Encoder;
import cn.hutool.core.util.CharsetUtil;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.*;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author du
 * @date 2021/1/2 17:51
 */
@Slf4j
public class FileDownloadUtils {
    /**
     * 文件下载
     * 不删除源文件
     *
     * @param filePath 文件地址
     * @param fileName 下载的文件名
     * @param response 响应体
     */
    public static void fileDownload(String filePath, String fileName,
                                    HttpServletResponse response, String mimeType) {
        response.setContentType((mimeType != null) ? mimeType
                : MediaType.APPLICATION_OCTET_STREAM_VALUE);
        response.setContentType(CharsetUtil.UTF_8);
        File file = new File(filePath);

        try (
                FileInputStream is = new FileInputStream(file);
                BufferedInputStream bs = new BufferedInputStream(is);
                ServletOutputStream os = response.getOutputStream();
                BufferedOutputStream bos = new BufferedOutputStream(os)
        ) {
            response.setHeader("Content-Disposition", "attachment; filename=\""
                    + URLEncoder.encode(fileName, StandardCharsets.UTF_8) + "\"");
            byte[] len = new byte[1024 * 2];
            int read;
            while ((read = bs.read(len)) != -1) {
                bos.write(len, 0, read);
            }
            bos.flush();
        } catch (IOException e) {

            log.error("文件下载异常", e);
        }
    }

    /**
     * 文件下载 使用nio 不删除源文件
     *
     * @param response            响应体
     * @param filePathAndFileName 文件地址
     * @param mimeType            媒体类型
     */
    public static void download(HttpServletResponse response,
                                String filePathAndFileName, String mimeType, String fileName)
            throws IOException {

        File file = new File(filePathAndFileName);
        response.setContentType((mimeType != null) ? mimeType
                : MediaType.APPLICATION_OCTET_STREAM_VALUE);
        response.setContentLength((int) file.length());
        String encode = URLEncoder.encode(fileName, StandardCharsets.UTF_8);
        response.setHeader("Content-disposition", "attachment;filename=" + encode);
        ServletOutputStream op = response.getOutputStream();
        int bufferSize = 131072;

        try (
                FileInputStream fileInputStream = new FileInputStream(file);
                FileChannel fileChannel = fileInputStream.getChannel()
        ) {
            ByteBuffer bb = ByteBuffer.allocateDirect(786432);
            byte[] byteArray = new byte[bufferSize];
            int nRead, nGet;
            while ((nRead = fileChannel.read(bb)) != -1) {
                if (nRead == 0) {
                    continue;
                }
                bb.position(0);
                bb.limit(nRead);
                while (bb.hasRemaining()) {
                    nGet = Math.min(bb.remaining(), bufferSize);
                    bb.get(byteArray, 0, nGet);
                    op.write(byteArray);
                }
                bb.clear();
            }
        } catch (IOException e) {
            //
        }
    }


    /**
     * Using ResponseEntity<InputStreamResource>
     *
     * @param filePath 文件的路径
     * @return 流
     */
    public static ResponseEntity<InputStreamResource> downloadResponseEntityByInputStreamResource(String filePath) throws IOException {

        File file = new File(filePath);
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment;filename=" + file.getName())
                .contentType(MediaType.APPLICATION_OCTET_STREAM).contentLength(file.length())
                .body(resource);
    }

    /**
     * Using ResponseEntity<ByteArrayResource>
     *
     * @param filePath 文件路径
     * @return 二进制
     */
    public static ResponseEntity<ByteArrayResource> downloadResponseEntityByByteArrayResource(String filePath) throws IOException {

        Path path = Paths.get(filePath);
        byte[] data = Files.readAllBytes(path);
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment;filename=" + path.getFileName().toString())
                .contentType(MediaType.APPLICATION_OCTET_STREAM).contentLength(data.length)
                .body(resource);
    }

    /**
     *
     * @param agent
     * @param filename
     * @return
     * @throws UnsupportedEncodingException
     */

    public static String getFileName(String agent, String filename)  {
        if (agent.contains("MSIE")) {
            // IE浏览器
            filename = URLEncoder.encode(filename, StandardCharsets.UTF_8);
            filename = filename.replace("+", " ");
        } else if (agent.contains("Firefox")) {
            // 火狐浏览器
            filename = "=?utf-8?B?" + Base64Encoder.encode(filename.getBytes(StandardCharsets.UTF_8)) + "?=";
        } else {
            // 其它浏览器
            filename = URLEncoder.encode(filename, StandardCharsets.UTF_8);
        }
        return filename;
    }


}
