package com.du.utils;

import cn.hutool.core.util.CharUtil;
import com.du.exceptions.my.FileFormatException;
import com.du.exceptions.my.FileUploadSizeException;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 常见的string工具类
 *
 * @author du
 * @date 2020/5/1 22:12
 */

public class MyStringUtils {

    private static final Pattern underlineToCamel = Pattern.compile("([A-Za-z\\d]+)(_)?");
    private static final Pattern camelToUnderline = Pattern.compile("[A-Z]([a-z\\d]+)?");

    /**
     * 判断一个字符串在另一个字符串中出现的次数 （特殊字符也能判断）
     *
     * @param str 原字符串
     * @param key 待判断的字符串
     * @return d
     */
    public static int getStringCount(String str, String key) {

        int count = 0;
        int index;
        while ((index = str.indexOf(key)) != -1) {
            count++;
            str = str.substring(index + key.length());
        }
        return count;
    }

    /**
     * 将字符串的首字母转成大写,其他内容转成小写
     *
     * @param str d
     * @return d
     */
    public static String toConvertLetterFirstUpper(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
    }

    /**
     * 获取字符串中大写字母，小写字母，数字的个数
     *
     * @param str d
     * @return d
     */
    public static Map<String, Integer> getUpperAndLowerDigit(String str) {
        int upper = 0;
        int lower = 0;
        int digit = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c >= 'A' && c <= 90) {
                upper++;
            } else if (c >= 97 && c <= 122) {
                lower++;
            } else if (c >= 48 && c <= '9') {
                digit++;
            }
        }
        Map<String, Integer> hashMap = new HashMap<String, Integer>(3);
        hashMap.put("upper", upper);
        hashMap.put("lower", lower);
        hashMap.put("digit", digit);
        return hashMap;
    }

    /**
     * 下划线转驼峰法
     *
     * @param line       源字符串
     * @param smallCamel 大小驼峰,是否为小驼峰
     * @return 转换后的字符串
     */
    public static String underlineToCamel(String line, boolean smallCamel) {
        if (line == null || "".equals(line)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        Matcher matcher = underlineToCamel.matcher(line);
        while (matcher.find()) {
            String word = matcher.group();
            sb.append(smallCamel && matcher.start() == 0 ? Character
                    .toLowerCase(word.charAt(0)) : Character.toUpperCase(word
                    .charAt(0)));
            int index = word.lastIndexOf(CharUtil.DASHED);
            if (index > 0) {
                sb.append(word.substring(1, index).toLowerCase());
            } else {
                sb.append(word.substring(1).toLowerCase());
            }
        }
        return sb.toString();
    }

    /**
     * 驼峰法转下划线
     *
     * @param line 源字符串
     * @return 转换后的字符串
     */
    public static String camelToUnderline(String line) {
        if (line == null || "".equals(line)) {
            return "";
        }
        line = String.valueOf(line.charAt(0)).toUpperCase()
                .concat(line.substring(1));
        StringBuilder sb = new StringBuilder();
        Matcher matcher = camelToUnderline.matcher(line);
        while (matcher.find()) {
            String word = matcher.group();
            sb.append(word.toUpperCase());
            sb.append(matcher.end() == line.length() ? "" : CharUtil.DASHED);
        }
        return sb.toString();
    }

    /**
     * 判断Object对象为空或空字符串
     *
     * @param obj d
     * @return d  d
     */
    public static Boolean isObjectEmpty(Object obj) {
        String str = null == obj ? "" : obj + "";
        return StringUtils.isBlank(str);

    }

    /**
     * 将一个字符串转为数组 带，分割
     *
     * @param s x
     * @return list
     */
    public static List<String> toList(String s) {
        return new ArrayList<>(Arrays.asList(s.split(",")));

    }


    /**
     * 文件后准名校验
     *
     * @param fileSuffixList 待验证的后准名集合
     * @param fileSuffix     文件的后准名
     */
    public static void checkFileSuffix(List<String> fileSuffixList, String fileSuffix) {
        if (!fileSuffixList.contains(fileSuffix)) {
            throw new FileFormatException();
        }
    }

    /**
     * 校验上传文件的大小
     *
     * @param relSize      文件真实的大小
     * @param standardSize 文件标准的大小
     */
    public static void checkUploadFileSize(long relSize, long standardSize) {
        if (relSize > standardSize) {
            throw new FileUploadSizeException("大小不能超过" + standardSize);
        }
    }


}
