package com.du.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


/**
 * 字母    	日期或时间元素	        表示	            示例
 * G	    Era 标志符	        Text	         AD
 * Y        Y week year :今天是2020年12月31号礼拜天,是这一周的第一天,到了明天,就已经是2021年1月1号了,那么今天的时间在转化年份的时候,也会显示为2021年
 * y	    年	Year day        1996 ;           96
 * M	    年中的月份	Month	July ; Jul ;      07
 * w	    年中的周数	Number	27
 * W	    月份中的周数	Number	2
 * D	    年中的天数	Number	189
 * d	    月份中的天数	Number	10
 * F	    月份中的星期	Number	2
 * E	    星期中的天数	Text	Tuesday ; Tue
 * a	    Am/pm 标记	Text	PM
 * H	    一天中的小时数（0-23）	Number	0
 * k	    一天中的小时数（1-24）	Number	24
 * K	    am/pm 中的小时数（0-11）	Number	0
 * h	    am/pm 中的小时数（1-12）	Number	12
 * m	    小时中的分钟数	Number	30
 * s	    分钟中的秒数	Number	55
 * S	    毫秒数	Number	978
 * z	    时区	General time zone	Pacific Standard Time ; PST ; GMT-08:00
 * Z	    时区	RFC 822 time zone	                -0800
 * @author du
 */
public class MyDateUtils {


    /**
     * 获取当前日期的年月日， 时分秒 24小时制
     */
    public static String getCurrentDate24() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * 获取当前日期的年月日， 时分秒 24小时制 中文
     */
    public static String getCurrentDateChines24() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH:mm:ss"));
    }

    /**
     * 获取当前日期的时分秒
     *
     * @return 当前日期的时分秒
     */
    public static String getCurrentDateHms() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss"));
    }

    /**
     * 获取当前日期的年月日， 时分秒 12小时制
     */
    public static String getCurrentDate12() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }
}
