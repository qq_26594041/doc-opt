package com.du.utils;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.HashMap;

/**
 * @author du
 * @date 2021/1/2 17:19
 */
public class ImagesUtils {

    /**
     * 私有获取图片长和宽的方法
     *
     * @param inputStream 流
     * @return height: 图片高度  width: 图片宽度
     */
    private static HashMap<String, Integer> getImagesWidthAndHeight(InputStream inputStream) throws Exception {

        BufferedImage read = ImageIO.read(inputStream);
        HashMap<String, Integer> retMap = new HashMap<>(2);
        retMap.put("height", read.getHeight());
        retMap.put("width", read.getWidth());
        return retMap;

    }

    public static HashMap<String, Integer> getImagesWidthAndHeightOut(InputStream inputStream) throws Exception {
        return getImagesWidthAndHeight(inputStream);
    }
}
