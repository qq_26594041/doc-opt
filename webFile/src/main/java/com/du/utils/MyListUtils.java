package com.du.utils;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * @author du
 * @Description TODO
 * @Date 2020/5/23 19:00
 * @Created by 杜瑞龙
 */
public class MyListUtils {

    public static ArrayList quChong(ArrayList<Object> a) {
        ArrayList<Object> b = new ArrayList();
        Iterator<Object> iterator = a.iterator();

        while (iterator.hasNext()) {
            Object next = iterator.next();
            if (!b.contains(next)) {
                b.add(next);
            }
        }
        return b;
    }
}
