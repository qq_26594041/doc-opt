package com.du.enums;


/**
 *
 * @author du
 */
public class Constants {

    /**
     * 获取项目根目录
     */
    public static final String PROJECT_ROOT_DIRECTORY = System.getProperty("user.dir");
    /**
     * DES加密的密钥
     */
    public  static final   String DES_KEY = "erwonuiwe87^grtewb";

    /**
     * 请求头 - token
     */
    public static final String REQUEST_HEADER = "X-Token";

}
