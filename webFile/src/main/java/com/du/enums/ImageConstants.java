package com.du.enums;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 图片的常量
 *
 * @author 杜瑞龙
 * @date 2021/1/7 22:18
 */
public class ImageConstants {
    /**
     * 上传图片时默认的contentType头部
     */
    public static final String IMAGE_UPLOAD_CONTENT_TYPE = "image";
    /**
     * 上传图片时返回注解的标志，与枚举相对应
     */
    public static final String IMAGE_UPLOAD_METHOD = "IMAGE_UPLOAD_SUCCESS";
    /**
     * 上传图片时：格式的验证
     */
    public static final ArrayList<String> COMMONS_IMG_FORMAT = new ArrayList<>(
            Arrays.asList("png", "jpg", "jpeg", "gif"));
    /**
     * 自定义的格式校验，业务相关
     */
    public static final ArrayList<String> SELECT_IMG_FORMAT = new ArrayList<>(
            Arrays.asList("png", "jpg"));
    /**
     * 上传图片时：图片限制的大小 60M 通用的验证
     */
    public static final long IMG_SIZE60M = 62914560;
}
