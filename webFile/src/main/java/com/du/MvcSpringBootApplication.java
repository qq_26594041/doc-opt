package com.du;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @author du
 */
@SpringBootApplication
public class MvcSpringBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(MvcSpringBootApplication.class, args);
    }

}
