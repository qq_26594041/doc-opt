package com.du.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author du
 * @date 2020/12/28 21:28
 */
@Configuration
@ConfigurationProperties(prefix = "uploads", ignoreUnknownFields = false)
@Getter
@Setter
public class UploadConfig {

    private String savePathImag;
    private String viewImgPathPrefix;
}
