package com.du.config;


import com.du.utils.MyFileUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author 杜瑞龙
 * @date 2022/1/15 10:26
 */
@Configuration
@RequiredArgsConstructor
public class FileMvcConifg implements WebMvcConfigurer {
    private final UploadConfig uploadConfig;

    /**
     * 虚拟路径的添加
     * @param registry d
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(uploadConfig.getViewImgPathPrefix()+"**")
                .addResourceLocations("file:" + MyFileUtils.getPackerJarOrWarPath() + uploadConfig.getSavePathImag() )
                .resourceChain(false);
        registry.setOrder(1);
    }

}
