package AdministrativeArea;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.du.bean.AdministrativeArea;
import com.du.utils.AdministrativeAreaUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;


/**
 * @author 杜瑞龙
 * @date 2021/3/10 23:29
 */
public class AdministrativeAreaTest {
    @Test
    public void test() throws IOException {
        List<AdministrativeArea> province = AdministrativeAreaUtils.getProvince("2020");
        System.out.println();
    }
    @Test
    public void test1()  {


        String filePath="/pcx/";
        String country="00000";
        String province="0000";
        String city="000";
        String town="00";
        String cpcSuffix=  "_full.json";
        String county=".json";
        String property = System.getProperty("user.dir")+"/all_datav.json";
        JSONArray objects = JSONUtil.readJSONArray(new File(property), StandardCharsets.UTF_8);
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "80");
        objects.parallelStream().forEach(
                e->{
                    String name ="";
                    String  ip="https://geo.datav.aliyun.com/areas_v2/bound/";
                    JSONObject area = JSONUtil.parseObj(e);
                    String adcode = area.getStr("adcode");
                    if (StrUtil.endWithAny(adcode, country, province, city,town)){
                        ip=ip+adcode+cpcSuffix;
                        name=filePath+name+adcode+cpcSuffix;
                    }else{
                        ip=ip+adcode+county;
                        name=filePath+name+adcode+county;
                    }
                    byte[] bytes = HttpUtil.get(ip).getBytes(StandardCharsets.UTF_8);
                    FileUtil.writeBytes(bytes,name);
                }
        );
    }
}
