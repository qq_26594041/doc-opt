package doc;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.net.URL;

/**
 * @Description TODO
 * @Date 2020/12/4 10:09
 * @Created by 杜瑞龙
 */
public class WeChatTest {

    @Test
    public void test() throws IOException {
        URL url = new URL("http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2020/");
        Document document = Jsoup.parse(url, 10000);
        // 文章出处（作者）
        String referName = document.getElementsByClass("profile_nickname").get(0).text();
        // 文章封面图链接
        String coverUrl = document.select("meta[property=\"og:image\"]").get(0).attr("content");
        // 文章标题
        String title = document.getElementById("activity-name").text();
        // 文章内容
        Element content = document.getElementsByClass("rich_media_area_primary_inner").get(0);
        System.out.println(document);
    }
}
