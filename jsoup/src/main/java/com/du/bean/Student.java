package com.du.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author 杜瑞龙
 * @date 2022/8/16 18:34
 */
@Data
@AllArgsConstructor
public class Student {

    public String name;
    public String show;
    public  int age;


}
