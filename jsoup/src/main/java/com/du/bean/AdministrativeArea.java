package com.du.bean;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @date 2021/3/10 8:57
 * @author 杜瑞龙
 */

/**
 * 行政区划表  省市县，town 000000000000
 * @author du
 */
@Data
public class AdministrativeArea {


    /**
     * 行政编码 也是主键
     */
    private String code;

    /**
     * 行政名称
     */
    private String name;

    /**
     * 上级编码
     */
    private String parCode;


    private List<AdministrativeArea> sub = new ArrayList<>();




}