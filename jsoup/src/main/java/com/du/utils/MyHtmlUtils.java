package com.du.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 此类为对html标签的处理
 * @author du
 * @date 2020/8/29 21:38
 */
public class MyHtmlUtils {


    /**处理html标签，替换图片的相对路径为绝对路径
     * @param url  html
     * @param htmlTag 图片的地址
     * @return  成功替换后的
     */
    public static String  changeImaPath(String url,String htmlTag){
        Element doc = Jsoup.parseBodyFragment(htmlTag).body();
        Elements pngs = doc.select("img[src]");
        for (Element element : pngs) {
            String imgUrl = element.attr("src");
            if (imgUrl.trim().startsWith("/")) {
                imgUrl =url + imgUrl;
                element.attr("src", imgUrl);
            }
        }
        return  doc.toString().replaceFirst("<body>","")
                .replaceFirst("</body>","").replaceAll("\n","");

    }


}
