package com.du.utils;

import com.du.bean.AdministrativeArea;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 杜瑞龙
 * @date 2021/3/10 23:04
 */
public class AdministrativeAreaUtils {
    public static String SITE_URL = "http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/";
    private static List<AdministrativeArea> regions = new ArrayList<>();

    public static List<AdministrativeArea> getProvince(String year) throws IOException {

        Document doc = Jsoup.connect(SITE_URL + year).get();
        Elements links = doc.select("tr.provincetr").select("a");
        links.forEach(e -> {
            AdministrativeArea region = new AdministrativeArea();
            String code = StringUtils.left(e.attr("href"), 2);
            region.setCode(StringUtils.rightPad(code, 6, '0'));
            region.setName(e.text());
            region.setParCode("0");
            String cityUrl = e.attr("abs:href");
            getCity(cityUrl, region);
            regions.add(region);

        });

        return regions;
    }

    /**
     * 获取市地址
     *
     * @param url
     * @param region
     */
    private static void getCity(String url, AdministrativeArea region) {
        try {
            Document doc = Jsoup.connect(url).get();
            Elements links = doc.select("tr.citytr");
            for (Element e : links) {
                AdministrativeArea city = new AdministrativeArea();
                Elements alist = e.select("a");
                Element codeE = alist.get(0);
                Element codeN = alist.get(1);
                String code = codeE.text();
                String name = codeN.text();
                if ("市辖区".equals(name)) {
                    name = region.getName();
                }
                city.setCode(StringUtils.left(code,6));
                city.setName(name);
                String absHref = codeE.attr("abs:href");
                getArea(absHref, city);
                region.getSub().add(city);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取区县地址
     *
     * @param url
     * @param region
     */
    private static void getArea(String url, AdministrativeArea region) throws IOException {


        Document doc = Jsoup.connect(url).get();
        Elements links = doc.select("tr.countytr");

        for (Element e : links) {
            AdministrativeArea area = new AdministrativeArea();
            Elements alist = e.select("a");
            if (alist.size() > 0) {
                Element codeE = alist.get(0);
                String code = codeE.text();
                area.setCode(StringUtils.left(code,6));
                Element codeN = alist.get(1);
                String name = codeN.text();
                area.setName(name);

            } else {
                alist = e.select("td");
                area.setCode(alist.get(0).text());
                area.setName(alist.get(1).text());
            }
            region.getSub().add(area);
        }
    }
//        //乡镇
//        private static void getTown(String url, RegionEntry region) {
//            Document doc;
//            try {
//                doc = Jsoup.connect(url).get(); // Jsoup.connect(url).get();
//                //<tr class='towntr'><td><a href='07/110107001.html'>110107001000</a></td><td><a href='07/110107001.html'>八宝山街道办事处</a></td></tr>
//                Elements links = doc.select("tr.towntr");
//                RegionEntry town;
//                for (Element e : links) {
//                    town = new RegionEntry();
//                    Elements alist = e.select("a");
//                    if (alist.size() > 0) {
//                        Element codeE = alist.get(0);
//                        String code = codeE.text();
//                        town.setCode(code);
//                        Element codeN = alist.get(1);
//                        String name = codeN.text();
//                        town.setName(name);
//                        region.getSub().add(town);
//                    } else {
//                        alist = e.select("td");
//                        town.setCode(alist.get(0).text());
//                        town.setName(alist.get(1).text());
//                        region.getSub().add(town);
//                    }
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//}
}

